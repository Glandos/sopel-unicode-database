Sopel Unicode database plugin
=============================

You can use this to find a Unicode character from part of its description:

```
.unicode cedilla                                                                                                                                                                               |
> 5 out of 29                                                                                                                                                                                       |
> - CEDILLA (¸ escaped as b'\\xb8')                                                                                                                                                                 |
> - COMBINING CEDILLA (̧ escaped as b'\\u0327')                                                                                                                                                      |
> - COMBINING LATIN SMALL LETTER C CEDILLA (ᷗ escaped as b'\\u1dd7')                                                                                                                                |
> - LATIN CAPITAL LETTER C WITH CEDILLA (Ç escaped as b'\\xc7')                                                                                                                                     |
> - LATIN CAPITAL LETTER C WITH CEDILLA AND ACUTE (Ḉ escaped as b'\\u1e08')      
```

Ok, the output is not nice for the moment… And, of course, results are limited to 5, to avoid flooding.

Requirements
------------
Nothing. Just Python standard library.

Initial setup
-------------
Just run, with admin privileges: `.unicode_rebuild`.
You should have write access to ./unicode.db. Create an empty file with needed permissions if necessary.