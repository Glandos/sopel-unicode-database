#!/usr/bin/env python3

import unicodedata
import marshal
import re

from pathlib import Path

from sopel.module import commands, require_admin
from sopel import formatting


MAX_LISTED_RESULTS = 5
MAX_LINE_CHARACTER = 400
MAX_LINE_RESULTS = 3
CODE_POINT_RE = re.compile(r"(?:(?:u|U)\+)?(?:(?P<code>[0-9a-fA-F]{4,8}))")
ESCAPED_SINGLE_CHAR_RE = re.compile(r'"(?P<character>.)"')


def build_unicode(file):
    data = []
    for i in range(0x110000):
        char = chr(i)
        name = unicodedata.name(char, "")
        if name:
            data.append(name)
    marshal.dump(frozenset(data), file)


def find_name(db, name):
    return [match for match in db if name in match]


def get_code_point(char):
    return "U+{0:0>4}".format(hex(ord(char))[2:].upper())


def get_char_repr(char):
    return "{circle}{char}{circle}".format(
        circle="" if unicodedata.combining(char) == 0 else "\u202F", char=char
    )


def message_splitter(names, joiner=", ", max_len=400, transform=None):
    splitted_names = []
    current_len = 0
    previous = 0
    joiner_len = len(joiner)
    for i, name in enumerate(names):
        if transform is not None:
            name = transform(name)
            names[i] = name
        current_len += len(name) + joiner_len
        if current_len > max_len:
            splitted_names.append((i - previous, joiner.join(names[previous:i])))
            previous = i
            current_len = len(name) + joiner_len
    splitted_names.append(
        (
            len(names) - previous,
            joiner.join(
                transform(name) if transform else name for name in names[previous:]
            ),
        )
    )
    return splitted_names


def unicode_find(bot, search):
    """
    Find all Unicode name that matches
    """
    db_search = search.upper()
    with Path(__file__).parent.joinpath("unicode.db").open("rb") as file:
        db = marshal.load(file)
        names = find_name(db, db_search)
        if names:

            def colorize(name):
                return name.replace(
                    db_search, formatting.color(db_search, fg=formatting.colors.ORANGE)
                )

            names.sort()
            prefix = "- "
            result_count = len(names)
            if result_count == 1:
                name = colorize(names[0])
                unicode_info(bot, unicodedata.lookup(names[0]), name)
            elif result_count <= MAX_LISTED_RESULTS:
                bot.say("{} results".format(result_count))
                for name in names:
                    char = unicodedata.lookup(name)
                    unicode_info(bot, char, colorize(name), prefix)
            else:
                lines = message_splitter(names, transform=colorize)
                line_count = len(lines)
                if line_count > MAX_LINE_RESULTS:
                    display_count = sum(line[0] for line in lines[:MAX_LINE_RESULTS])
                    bot.say("{} out of {} results".format(display_count, result_count))
                else:
                    bot.say("{} results".format(result_count))
                for line in lines[:MAX_LINE_RESULTS]:
                    bot.say(line[1])

        else:
            bot.say("No match for {}".format(search))


def unicode_info(bot, char, name=None, prefix=""):
    bot.say(
        "{prefix}{name} ({char} {escaped})".format(
            prefix=prefix,
            name=name if name else unicodedata.name(char, "<No name>"),
            char=get_char_repr(char),
            escaped=get_code_point(char),
        )
    )


def show_help(bot):
    bot.say("Find an Unicode character using part of its name: .u HAMMER")
    bot.say("Show information about one Unicode character: .u ℀")
    bot.say(
        'Show information about one Unicode character (using quotes for spacing characters): .u " "'
    )


@commands("unicode", "u")
def unicode(bot, trigger):
    trigger = trigger.group(2)
    if not trigger:
        show_help(bot)
        return
    code_point_matcher = CODE_POINT_RE.fullmatch(trigger)
    single_char_matcher = ESCAPED_SINGLE_CHAR_RE.fullmatch(trigger)
    if len(trigger) == 1:
        unicode_info(bot, trigger)
    elif single_char_matcher:
        character = single_char_matcher.group("character")
        unicode_info(bot, character)
    elif code_point_matcher:
        value = code_point_matcher.group("code")
        unicode_info(bot, chr(int(value, 16)))
    else:
        unicode_find(bot, trigger)


@require_admin
@commands("unicode_rebuild")
def unicode_rebuild(bot, trigger):
    """
    Rebuilt the Unicode name database
    """
    with Path(__file__).parent.joinpath("unicode.db").open("w+b") as file:
        build_unicode(file)
        bot.say("Rebuilt database")
